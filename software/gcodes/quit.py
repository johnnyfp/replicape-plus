'''
GCode G21
Setting units

Author: Mathieu Monney
email: zittix(at)xwaves(dot)net
Website: http://www.xwaves.net
License: CC BY-SA: http://creativecommons.org/licenses/by-sa/2.0/
'''

from GCodeCommand import GCodeCommand

class quit(GCodeCommand):

    def execute(self,g):
        g.set_answer("Quitting") 


    def get_description(self):
        return "Remotely Exits Redeem"
